package messages;

import java.util.Optional;

import model.Pattern;

public class SettingsMsg {
	private Optional<Integer> size = Optional.empty();
	private Optional<Pattern> pattern = Optional.empty();
	private Optional<Integer> waitMs = Optional.empty();
	//private Optional<Integer> bulk = Optional.empty();	


	private SettingsMsg(Optional<Integer> size, Optional<Pattern> pattern, Optional<Integer> waitMs
			/*, Optional<Integer> bulk*/) {
		this.size = size;
		this.pattern = pattern;
		this.waitMs = waitMs;
		//this.bulk = bulk;
	}

	public Optional<Integer> getSize() {
		return size;
	}

	public Optional<Pattern> getPattern() {
		return pattern;
	}

	public Optional<Integer> getWaitMs() {
		return waitMs;
	}

	/*
	public Optional<Integer> getBulk() {
		return bulk;
	}
	*/

	public static class Builder {
		private Optional<Integer> size = Optional.empty();
		private Optional<Pattern> pattern = Optional.empty();
		private Optional<Integer> waitMs = Optional.empty();
		//private Optional<Integer> bulk = Optional.empty();	

		public Builder size(int size) {
			this.size = Optional.ofNullable(size);
			return this;
		}

		public Builder pattern(Pattern pattern) {
			this.pattern = Optional.ofNullable(pattern);
			return this;
		}

		public Builder waitMs(int waitMs) {
			this.waitMs = Optional.ofNullable(waitMs);
			return this;
		}
		
		public SettingsMsg build() {
			if (this.size.isPresent() && this.pattern.isPresent() && this.waitMs.isPresent())
				return new SettingsMsg(this.size, this.pattern, this.waitMs);
			else
				return null;
			//throw new Exception();
		}
	}
}
