package messages;

import java.util.List;

import akka.actor.ActorRef;

public class NeighborMsg {
	
	private final List<ActorRef> neighbours;
	private final ActorRef controller;

	public NeighborMsg(List<ActorRef> neighbours, ActorRef controller) {
		this.neighbours = neighbours;
		this.controller = controller;
	}
	
	public List<ActorRef> getNeibourgh() {
		return this.neighbours;
	}
	
	public ActorRef getController() {
		return this.controller;
	}

}
