package messages;

import javafx.stage.Stage;

public class InitMsg {
	
	private final Stage primaryStage;
	private final SettingsMsg settings;

	public InitMsg(Stage primaryStage, SettingsMsg settings) {
		this.primaryStage = primaryStage;
		this.settings = settings;
	}

	public Stage getStage() {
		return primaryStage;
	}
	
	public SettingsMsg getSettings() {
		return settings;
	}

}
