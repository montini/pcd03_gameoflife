package messages;

public class MatrixMsg {
	private final byte[][] matrix;

	public MatrixMsg(byte[][] matrix) {
		super();
		this.matrix = matrix;
	}

	public byte[][] getMatrix() {
		return matrix;
	}
	
}
