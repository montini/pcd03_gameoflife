package messages;

public enum BasicMessages {
	
	NEIGHOK,
	START,
	STOP,
	NEXTGEN,
	CHECKNEIGH,
	PRINT,
	VIEWDONE,
	AWAITDONE;

}
