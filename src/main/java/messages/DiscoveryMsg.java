package messages;

import akka.actor.ActorRef;

public class DiscoveryMsg {

	private final ActorRef ref;

	public DiscoveryMsg(ActorRef ref) {
		super();
		this.ref = ref;
	}
	
	public ActorRef getRef() {
		return this.ref;
	}
	
}
