package messages;

import java.util.Optional;

public class PrintMsg {
	private final byte[][] mat;
	private final int aliveCount;
	private final long time;
	private final double avgTime;
	private final int epoch;

	private PrintMsg(final byte[][] mat, final int aliveCount, final long time, final double avgTime, final int epoch) {
		this.mat = mat;
		this.aliveCount = aliveCount;
		this.time = time;
		this.avgTime = avgTime;
		this.epoch = epoch;
	}

	public byte[][] getMat() {
		return this.mat;
	}	
	
	public int getAliveCount() {
		return this.aliveCount;
	}
	
	public long getTime() {
		return this.time;
	}
	
	public double getAvgTime() {
		return this.avgTime;
	}
	
	public int getEpoch() {
		return this.epoch;
	}
	
	public static class Builder {
		private Optional<byte[][]> mat;
		private Optional<Integer> aliveCount;
		private Optional<Long> time;
		private Optional<Double> avgTime;
		private Optional<Integer> epoch;
		
		public Builder mat(byte[][] mat) {
			this.mat = Optional.ofNullable(mat);
			return this;
		}
		
		public Builder aliveCount(int aliveCount) {
			this.aliveCount = Optional.ofNullable(aliveCount);
			return this;
		}
		
		public Builder time(long time) {
			this.time = Optional.ofNullable(time);
			return this;
		}
		
		public Builder avgTime(double avgTime) {
			this.avgTime = Optional.ofNullable(avgTime);
			return this;
		}
		
		public Builder epoch(int epoch) {
			this.epoch = Optional.ofNullable(epoch);
			return this;
		}
		
		public PrintMsg build() {
			if (mat.isPresent() && aliveCount.isPresent() && time.isPresent() && epoch.isPresent() && avgTime.isPresent()) {
				return new PrintMsg(mat.get(), aliveCount.get(), time.get(), avgTime.get(), epoch.get());
			}
			return null;
		}
		
		
	}
	
}
