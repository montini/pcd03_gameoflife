package messages;

import model.Position;

public class StateMsg {
	
	private final byte state;
	private final Position position;
	
	public StateMsg(byte state, Position position) {
		this.state = state;
		this.position = position;
	}
	
	public byte getState() {
		return state;
	}
	
	public Position getPosition() {
		return position;
	}

}
