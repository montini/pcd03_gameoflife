package main;

import java.util.ArrayList;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActorWithStash;
import messages.BasicMessages;
import messages.DiscoveryMsg;
import messages.MatrixMsg;
import messages.NeighborMsg;
import messages.PrintMsg;
import messages.SettingsMsg;
import messages.StateMsg;
import model.Position;
import model.DefaultSettings;

public class PeaceMakerActor extends AbstractActorWithStash {

	private ActorRef gui;
	private ActorRef[][] grid;
	private byte[][] world;
	private int updatedCells;
	private int cellsCount;
	private int aliveCount;
	private int size;
	private int waitMs;
	
	private int epoch;
	private Chrono cron;
	private List<Long> avgTime;
	private boolean running;
	
	@Override
	public Receive createReceive() {
		return receiveBuilder().match(DiscoveryMsg.class, msg -> {
			this.gui = msg.getRef();
		}).match(SettingsMsg.class, msg -> {
			this.size = msg.getSize().orElse(DefaultSettings.SIZE);
			this.waitMs = msg.getWaitMs().orElse(DefaultSettings.WAIT_MS);

			this.avgTime = new ArrayList<>();
			this.cron = new Chrono();
			
			GameOfLife.gameLog("Request Matrix creation");
			
			getContext().actorOf(Props.create(SettingsActor.class)).tell(msg, getSelf());
			
		}).match(MatrixMsg.class, msg -> {
			this.world = msg.getMatrix();
			this.cellsCount = this.size * this.size;
			this.grid = new ActorRef[this.size][this.size];
			for (int i = 0; i < this.size; i++) {
				for (int j = 0; j < this.size; j++) {
					grid[i][j] = getContext()
							.actorOf(Props.create(CellActor.class, new Position(i,j), this.world[i][j]));
				}
			}
			GameOfLife.gameLog("Cell actors created");

			this.running = false;
			
			for (int i = 0; i < this.size; i++) {
				for (int j = 0; j < this.size; j++) {
					List<ActorRef> neighbours = discoverNeighbors(i, j);
					grid[i][j].tell(new NeighborMsg(neighbours, getSelf()), getSelf());
				}
			}
			
			this.gui.tell(BasicMessages.NEIGHOK, getSelf());


			GameOfLife.gameLog("Neighbor list updated for each actor");
			
			getContext().become(this.pausedReceive());
			unstashAll();
		}).matchAny(any -> stash()).build();
	}


	private Receive livingReceive() {
		return receiveBuilder().match(StateMsg.class, msg -> {
			byte state = msg.getState();
			
			this.updatedCells++;
			this.aliveCount += state;
			
			this.world[msg.getPosition().getX()][msg.getPosition().getY()] = state;
			
			if (this.updatedCells == this.cellsCount) {
				this.cron.stop();
				this.epoch++;
				this.avgTime.add(this.cron.getTime());
				PrintMsg printMessage = new PrintMsg.Builder().mat(this.world)
						.aliveCount(this.aliveCount).time(this.cron.getTime())
						.avgTime(this.getAvg(this.avgTime))
						.epoch(this.epoch).build();
				this.gui.tell(printMessage, getSelf());
				this.updatedCells = 0;
				this.aliveCount = 0;
			}
		}).matchEquals(BasicMessages.VIEWDONE, m -> {
			Thread.sleep(this.waitMs);
			scala.concurrent.duration.FiniteDuration wait = scala.concurrent.duration.FiniteDuration.create(this.waitMs, "millis");
			getContext().getSystem().scheduler()
			.scheduleOnce(wait, getSelf(), BasicMessages.AWAITDONE, getContext().getSystem().dispatcher(), getSelf());//.scheduleOnce(null, gui, Duration.ofMillis(this.waitMs), null, gui)
		}).matchEquals(BasicMessages.AWAITDONE, m -> {
			if (this.running) {
				unstashAll();
				this.cron.start();
				for (int i = 0; i < this.size; i++) {
					for (int j = 0; j < this.size; j++) {
						grid[i][j].tell(BasicMessages.NEXTGEN, getSelf());
					}
				}
			} else {
				getContext().become(this.pausedReceive());
			}
		}).matchEquals(BasicMessages.STOP, m -> {
			this.running = false;
		}).matchAny(m -> stash()).build();
	}

	private Receive pausedReceive() {
		return receiveBuilder().matchEquals(BasicMessages.START, m -> {
			this.running = true;
			getContext().become(livingReceive());
			getSelf().tell(BasicMessages.VIEWDONE, ActorRef.noSender());
		}).build();
	}

	private List<ActorRef> discoverNeighbors(int x, int y) {
		List<ActorRef> neighbors = new ArrayList<>();
		for (int i = x-1; i<x+2; i++) {
			for (int j = y-1; j < y+2; j++) {
				if (!(i==x && j==y)) {
					try {
						neighbors.add(grid[i][j]);
					} catch (ArrayIndexOutOfBoundsException e) { }
				}
			}
		}
		return neighbors;
	}
	
	private double getAvg(List<Long> list) {
		return Math.round(list.stream().mapToDouble(d -> d).average().getAsDouble()*100.0)/100.0;
	}
}
