package main;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import javafx.application.Application;
import javafx.stage.Stage;
import messages.DiscoveryMsg;
import messages.InitMsg;
import messages.SettingsMsg;
import view.SettingsGUI;
import view.GameGUIActor;

public class GameOfLife {

	private static boolean hasStarted = false;
	private static long lastLogCall = System.currentTimeMillis();

	public static void main(String[] args) {
		GameOfLife.gameLog("Application launched");
		Application.launch(SettingsGUI.class);
	}

	public static void launchGame(SettingsMsg settings, Stage primaryStage) throws RuntimeException {
		new Thread(() ->  {
			if (!GameOfLife.hasStarted) {
				GameOfLife.gameLog("Starting main actors");
				GameOfLife.hasStarted = true;
				ActorSystem system = ActorSystem.create("GameOfLife");

				ActorRef gui = system.actorOf(Props.create(GameGUIActor.class));
				ActorRef controller = system.actorOf(Props.create(PeaceMakerActor.class));

				gui.tell(new DiscoveryMsg(controller), ActorRef.noSender());
				controller.tell(new DiscoveryMsg(gui), ActorRef.noSender());

				gui.tell(new InitMsg(primaryStage, settings), ActorRef.noSender());
				controller.tell(settings, ActorRef.noSender());
				GameOfLife.gameLog("Main actors started");
				
			} else {
				throw new RuntimeException("Game has already started, cannot start it twice!");
			}
		}).start();
	}


	public static void unrecognizedMessage(String caller) {
		System.err.println(caller + " has just received an unrecognized message!");
	}
	
	
	public synchronized static void gameLog(String str) {
		System.out.println("["+ (System.currentTimeMillis() - GameOfLife.lastLogCall) +"ms] - " + str);
		GameOfLife.lastLogCall = System.currentTimeMillis();
	}

}
