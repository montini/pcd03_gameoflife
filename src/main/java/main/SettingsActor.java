package main;

import java.util.Arrays;
import java.util.Random;

import akka.actor.AbstractActor;
import messages.MatrixMsg;
import messages.SettingsMsg;

public class SettingsActor extends AbstractActor {

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(SettingsMsg.class, msg -> {
			int size = msg.getSize().get();
			
			byte[][] matrix = new byte[size][size];
			for (int i = 0; i < size; i++)
				Arrays.fill(matrix[i], (byte) 0);
			
			Random random = new Random(840488);
			switch (msg.getPattern().get()) {
			case EMPTY:	
				
			break;
			case BLINKER:
				matrix[0][0] = 1;
				matrix[0][1] = 1;
				matrix[1][0] = 1;
				
				matrix[3][3] = 1;
				matrix[3][2] = 1;
				matrix[3][1] = 1;
				
				matrix[5][7] = 1;
				matrix[5][8] = 1;
				matrix[5][9] = 1;
				
				matrix[20][7] = 1;
				matrix[20][8] = 1;
				matrix[20][9] = 1;
				
			break;
			case RANDOM:
				for (int i = 0; i < size; i++)
					for (int j = 0; j < size; j++)
						matrix[i][j] = (byte) (random.nextDouble() < 0.165 ?  1 : 0);
			break;
			
			case LINES:
				random.setSeed(System.nanoTime());
				for (int i = 0; i < size; i+=55) {
					for (int j = 0; j < size; j++) {
						matrix[i][j] = (byte) (random.nextDouble() < 0.80 ?  1 : 0);
					}
				}
				for (int i = 0; i < size; i++) {
					for (int j = 0; j < size; j+=55) {
						matrix[i][j] = (byte) (random.nextDouble() < 0.80 ?  1 : 0);
					}
				}
				break;
			}
			
			getSender().tell(new MatrixMsg(matrix), getSelf());
		}).build();
	}

}
