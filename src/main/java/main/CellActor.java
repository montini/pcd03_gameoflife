package main;

import java.util.List;

import akka.actor.AbstractActorWithStash;
import akka.actor.ActorRef;
import messages.BasicMessages;
import messages.NeighborMsg;
import messages.StateMsg;
import model.Position;

public class CellActor extends AbstractActorWithStash {

	private final Position position;
	private List<ActorRef> neighbours;
	private ActorRef controller;
	private int receivedStates;
	private int neighboursAlive;
	private byte current;
	private byte next;

	public CellActor(Position position, byte start) {
		this.position = position;
		this.current = start;
		this.next = start;
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(NeighborMsg.class, msg -> {
			this.neighbours = msg.getNeibourgh();
			this.controller = msg.getController();
			
			getContext().become(this.livingReceive());
		}).build();
	}

	private Receive livingReceive() {
		return receiveBuilder().matchEquals(BasicMessages.NEXTGEN, m -> {
			this.neighbours.forEach(n -> n.tell(BasicMessages.CHECKNEIGH, getSelf()));
			this.receivedStates = 0;
			this.neighboursAlive = 0;
			this.current = this.next;
		
		    unstashAll();
			getContext().become(this.evaluatingReceive());
		}).matchEquals(BasicMessages.PRINT, m -> {
			System.out.println("Cell " + position.toString() + ": " + this.neighbours.size());
		}).matchAny(any -> stash()).build();
	}
	
	private Receive evaluatingReceive() {
		return receiveBuilder().match(StateMsg.class, msg -> {
			this.receivedStates++;

			this.neighboursAlive += msg.getState();

			if (this.receivedStates == this.neighbours.size()) {
				this.next = getNextGen();
				this.controller.tell(new StateMsg(this.next, position), getSelf());
				unstashAll();
				getContext().become(this.livingReceive());
			}
		}).matchEquals(BasicMessages.CHECKNEIGH, m -> {
			getSender().tell(new StateMsg(this.current, this.position), getSelf());
		}).matchAny(any -> stash()).build();
	}
	
	
	private byte getNextGen() {	
		if(this.neighboursAlive < 2 || this.neighboursAlive > 3){
			return 0;
		}else if(this.neighboursAlive == 3){
			return 1;
		}
		return this.current;
	}

}
