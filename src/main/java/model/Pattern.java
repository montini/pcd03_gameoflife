package model;

public enum Pattern {
	EMPTY,
	RANDOM,
	BLINKER,
	LINES;
}
