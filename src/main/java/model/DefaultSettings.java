package model;

public class DefaultSettings {
	
	//TODO rimuovere memoria condivisa
	public static int SIZE = 200;
	public static Pattern PATTERN = Pattern.RANDOM;
	public static int NUM_THREADS = 5;
	public static int WAIT_MS = 500;
	public static int SCALE = 1;
	public static int BULK = 100;
	
}
