package view;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import javafx.application.Platform;
import main.GameOfLife;
import messages.BasicMessages;
import messages.DiscoveryMsg;
import messages.InitMsg;
import messages.PrintMsg;

public class GameGUIActor extends AbstractActor  {

	private ActorRef controller;
	private GameGUI gui;

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(DiscoveryMsg.class, msg -> {
			this.controller = msg.getRef();
		}).match(InitMsg.class, msg -> {
			this.gui = new GameGUI(this);
			Platform.runLater(() -> {
				msg.getStage().setScene(this.gui.init(msg.getStage(), msg.getSettings()));
			});
		}).matchEquals(BasicMessages.NEIGHOK, m -> {
			this.gui.initDone();
			getContext().become(this.livingReceive());
		}).matchAny(m -> {
			GameOfLife.unrecognizedMessage(this.getSelf().toString());
		}).build();
	}

	private Receive livingReceive() {
		return receiveBuilder().match(PrintMsg.class, msg -> {
			this.gui.refreshMatrix(msg.getMat());
			this.gui.updateEpoch(msg.getEpoch());
			this.gui.updateAlive(msg.getAliveCount());
			this.gui.updateElapsed(msg.getTime());
			this.gui.updateAvg(msg.getAvgTime());
			this.controller.tell(BasicMessages.VIEWDONE, getSelf());
		}).matchAny(m -> {
			GameOfLife.unrecognizedMessage(this.getSelf().toString());
		}).build();
	}
	
	public void sendStartMsg() {
		this.controller.tell(BasicMessages.START, getSelf());
	}
	
	public void sendStopMsg() {
		this.controller.tell(BasicMessages.STOP, getSelf());
	}
	
	
}
